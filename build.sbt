name := """de"""

version := "1.0"

scalaVersion := "2.10.4"

lazy val common = project

lazy val flink = project.dependsOn(common)

lazy val spark = project.dependsOn(common)

lazy val root = project.aggregate(common, flink, spark)