module DifferentialEvolution

    export generate_population, next_population

    function generate_population(agents::Int, dimension::Int)
        population = Array{Array{Float64}}(agents)
        for i in 1:agents
            population[i] = rand(dimension) 
        end
        population
    end

    function pick_agents(population::Array{Array{Float64}}, x::Int)
        (agents,) = size(population)
        a = rand(1:agents)
        while a == x
            a = rand(1:agents)
        end
        b = rand(1:agents)
        while b == x || b == a
            b = rand(1:agents)
        end
        c = rand(1:agents)
        while c == x || c == b || c == a
            c = rand(1:agents)
        end
        tuple(population[a], population[b], population[c])
    end

    function next_population(population::Array{Array{Float64}}, score, f::Float64, cr::Float64)
        (agents,) = size(population)
        (dimensions,) = size(population[1])
        new_population = Array{Array{Float64}}(agents)
        for i in 1:agents
            agent = population[i]
            (a, b, c) = pick_agents(population, i)
            r = rand(1:dimensions)
            new_agent = zeros(dimensions)
            for (j, x) in enumerate(agent)
                if rand() < cr || j == r
                    new_agent[j] = a[j] + f * (b[j] - c[j])
                else
                    new_agent[j] = x
                end
            end
            new_population[i] = score(new_agent) < score(agent) ? new_agent : agent
        end
        new_population
    end
end