# ParallelDataTransfer not supported until 0.5
module Communication

    export getfrom, sendto

    getfrom(p::Int, nm::Symbol; mod=Main) = fetch(@spawnat(p, getfield(mod, nm)))
    
    function sendto(p::Int; args...)
        for (nm, val) in args
            @spawnat(p, eval(Main, Expr(:(=), nm, val)))
        end
    end

end