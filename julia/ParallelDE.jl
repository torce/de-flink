@everywhere using DifferentialEvolution
@everywhere using Communication

@everywhere begin 
    score = x -> x[1]*x[1] + x[2]*x[2]
    best_score = Inf
    epsilon = 0
    max_iterations = 100
    population_size = 30
    dimension = 2
    f = 0.8
    cr = 0.9
    islands = size(workers())[1]
end

function partition(complete_population::Array{Array{Float64}}, islands::Int)
    shuffle!(complete_population)
    partitions = Array{Array{Array{Float64}}}(islands)
    for i in 1:islands
        partitions[i] = complete_population[((i - 1) * population_size + 1):(i * population_size)]
    end
    partitions
end

@everywhere population = generate_population(population_size, dimension)

iterations = 0
while iterations < max_iterations
    @everywhere population = next_population(population, score, f, cr)
    # calculate best score and best agent
    complete_population = Array{Array{Float64}}(0)
    for i in workers()
        append!(complete_population, getfrom(i, :population))
    end
    partitions = partition(complete_population, islands)
    for i in 1:islands
        sendto(workers()[i], population=partitions[i]) 
    end
    iterations += 1
end
population