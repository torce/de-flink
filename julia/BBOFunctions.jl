type BBOFunction
    apply::Function
    domainMin::Float64
    domainMax::Float64
    min::Float64
end

Ackley = BBOFunction(-30, 30, 0) do agent
    sumsq = 0.0
    sumcos = 0.0
    for (i, value) in enumerate(agent)
        sumsq += value * value
        sumcos += cos(2 * pi * value)
    end
    -20 *exp(-0.2 * sqrt(sumsq / size(agent)[1])) - exp(sumcos / size(agent)[1]) + 20 + e
end

Griewank = BBOFunction(-600, 600, 0) do agent
    sumsq = 0.0
    prod = 1.0
    for (i, value) in enumerate(agent)
        sumsq += value * value
        prod *= cos(value / sqrt(i))
    end
    1 + sumsq * (1.0/4000.0) - prod
end

Quadric = BBOFunction(-100, 100, 0) do agent
    sumsq = 0.0
    for i in 1:(length(agent))
        sum = 0.0
        for j in 1:i
            sum += agent[j]
        end
        sumsq += sum * sum
    end
    sumsq
end

Rastrigin = BBOFunction(5.12, 5.12, 0) do agent
    acc = 0.0
    for (i, value) in enumerate(agent)
        acc += value * value - 10 * cos(2 * pi * value) + 10
    end
    acc
end

Rosenbrock = BBOFunction(-2.048, 2.048, 0) do agent
    acc = 0.0
    for i in 2:(1 + length(agent) / 2)
        a = agent((i << 1) - 1)
        b = agent((i << 1) - 2)
        acc += 100 * (a - b * b) * (a - b * b) + (1 - b) * (1 - b)
    end
    acc
end


Schwefel = BBOFunction(-500, 500, 0) do agent
    sum = 0.0
    outside = false
    for (i, value) in enumerate(agent)
        if(value < -500 || value > 500)
            outside = true
        end
        sum += value * sin(sqrt(abs(value)))
    end
    sum += length(agent) * 4.18982887272434686131e+02
    if(outside)
        sum += 1e50
    end
    sum
end

Spherical = BBOFunction(-100, 100, 0) do agent
    sumsq = 0.0
    for (i, value) in enumerate(agent)
        sumsq += value * value
    end
    sumsq
end

BBOFunctions = [Ackley, Griewank, Quadric, Rastrigin, Rosenbrock, Schwefel, Spherical]