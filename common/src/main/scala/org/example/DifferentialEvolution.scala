package org.example
import org.example.DifferentialEvolution.Population

import scala.util.Random

case class Agent(id: Int, vector: Seq[Double])

object DifferentialEvolution {
  type Population = Seq[Agent]

  def buildPopulation(populationSize: Int, dimension: Int, min: Double, max: Double): Population = {
    (0 until populationSize).map(i => randomAgent(i, dimension, min, max))
  }

  protected def randomAgent(id: Int, dimension: Int, min: Double, max: Double): Agent = {
    Agent(id, (0 until dimension).map(_ => min + Random.nextDouble() * (max - min)))
  }
}

class DifferentialEvolution(iterations: Int, score: BBOFunction, f: Double, cr: Double)
  extends (Population => Population) with Serializable {

  private def randomInt(min: Int, max: Int) = min + Random.nextInt(max - min)
  private def randomDouble(min: Double, max: Double) = min + Random.nextDouble() * (max - min)

  def apply(population: Population): Population = {
    //if (population.size < 4) throw new NotEnoughAgentsException()
    (0 until iterations).foldLeft(population)((p, _) => nextPopulation(p))
  }

  private def pickAgents(population: Population, x: Int, dimension: Int): (Agent, Agent, Agent) = {
    var a: Int = Random.nextInt(population.size)
    val agentA = if (population.size > 1) {
      while (a == x) { a = Random.nextInt(population.size) }
      population(a)
    } else {
      DifferentialEvolution.randomAgent(-1, dimension, score.domainMin, score.domainMax)
    }

    var b: Int = Random.nextInt(population.size)
    val agentB = if (population.size > 2) {
      while (b == x || b == a) { b = Random.nextInt(population.size) }
      population(b)
    } else {
      DifferentialEvolution.randomAgent(-1, dimension, score.domainMin, score.domainMax)
    }

    var c: Int = Random.nextInt(population.size)
    val agentC = if (population.size > 3) {
      while (c == x || c == a || c == b) { c = Random.nextInt(population.size) }
      population(c)
    } else {
      DifferentialEvolution.randomAgent(-1, dimension, score.domainMin, score.domainMax)
    }

    (agentA, agentB, agentC)
  }

  private def nextPopulation(population: Population): Population = {
    population.zipWithIndex.map {
      case (agent, index) =>
        val (a, b, c) = pickAgents(population, index, agent.vector.size)
        val r = randomInt(0, agent.vector.size)
        val y: Seq[Double] = agent.vector.zipWithIndex.map(e =>
          if (randomDouble(0, 1) < cr || e._2 == r)
            normalize(a.vector(e._2) + f * (b.vector(e._2) - c.vector(e._2)))
          else
            e._1
        )
        if (score(y) < score(agent.vector)) Agent(agent.id, y) else agent
    }
  }

  private def normalize(value: Double): Double = {
    if (value > score.domainMax)
      score.domainMax
    else if (value < score.domainMin)
      score.domainMin
    else
      value
  }
}
