package org.example

case class ExecutionParameters(score: BBOFunction = BBOFunctions(0),
                               dimension: Int = 10,
                               globalIterations: Option[Int],
                               iterations: Int = 100,
                               islands: Int = 4,
                               islandIterations: Int,
                               population: Int = 1000,
                               f: Double = 0.8,
                               cr: Double = 0.9,
                               epsilon: Option[Double])
