package org.example

import org.rogach.scallop.ScallopConf

class Conf(args: Seq[String]) extends ScallopConf(args) with Serializable {
  val masterUrl = opt[String]() //Only for Apache Spark
  val function = opt[Int](required = true, validate = f => f >= 0 && f <= 6)
  val dimension = opt[Int](required = true, validate = _ > 0)
  val globalIterations = opt[Int](validate = _ > 0)
  val iterations = opt[Int](required = true, validate = _ > 0)
  val islands = opt[Int](required = true, validate = _ > 0)
  val islandIterations = opt[Int](required = true, validate = _ > 0)
  val population = opt[Int](required = true, validate = _ > 0)
  val f = opt[Double](default = Some(0.8))
  val cr = opt[Double](default = Some(0.9))
  val epsilon = opt[Double](validate = _ > 0)
  requireOne(globalIterations, epsilon)
  verify()

  def build: ExecutionParameters = {
    ExecutionParameters(BBOFunctions(function()), dimension(), globalIterations.toOption, iterations(), islands(),
      islandIterations(), population(), f(), cr(), epsilon.toOption)
  }
}
