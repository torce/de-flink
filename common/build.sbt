resolvers in ThisBuild ++= Seq(Resolver.mavenLocal)

name := """common"""

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.rogach" %% "scallop" % "2.0.7")

