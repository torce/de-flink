package org.example.spark

import org.apache.spark.{SparkConf, SparkContext}

object WordCount extends App {
  val conf = new SparkConf()
  val sc = new SparkContext(conf)

  val textFile = sc.textFile(args(0))
  val counts = textFile.flatMap(line => line.split(" "))
    .map(word => (word, 1))
    .reduceByKey(_ + _)
  counts.saveAsTextFile(args(1))

  System.in.read
}
