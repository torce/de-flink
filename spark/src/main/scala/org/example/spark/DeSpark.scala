package org.example.spark

import org.apache.spark.{Partitioner, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.log4j.Logger
import org.example.DifferentialEvolution.Population
import org.example.{Agent, BBOFunction, DifferentialEvolution, ExecutionParameters}

class DeSpark(ep: ExecutionParameters) {

  @transient lazy val log: Logger = Logger.getLogger(getClass.getName)

  def run(context: SparkContext): Unit = {
    log.info("Building population")
    var population = context.parallelize(DifferentialEvolution.buildPopulation(ep.population, ep.dimension, ep.score.domainMin, ep.score.domainMax)).keyBy(_.id)
    val deFunction = new DifferentialEvolution(ep.iterations, ep.score, ep.f, ep.cr)

    var globalIterations = ep.globalIterations.getOrElse(Int.MaxValue)
    var termination = false
    do {
      log.info(s"Starting global iteration, $globalIterations remaining")
      population = islandIterations(population, new RandomPartitioner(ep.islands), deFunction)
      val best = bestAgent(population, ep.score)
      log.info(s"Best agent: $best")
      globalIterations -= 1
      termination = if(ep.epsilon.isEmpty) false else math.abs(best._1 - ep.score.min) < ep.epsilon.get
    } while(globalIterations > 0 && !termination)

  }

  def islandIterations(data: RDD[(Int, Agent)], partitioner: Partitioner, deFunction: (Population => Population)): RDD[(Int, Agent)] = {
    var population = data
    var iterations = ep.islandIterations
    var termination = false
    do {
      log.info("Partitioning population and applying island DE")
      population = population.partitionBy(partitioner)
        .mapPartitions(p => deFunction.apply(p.map(_._2).toSeq).toIterator)
        .keyBy(_.id)
      log.info("Calculating best score")
      val best = bestScore(population, ep.score)
      log.info(s"Best score: $best")
      iterations -= 1
      termination = if(ep.epsilon.isEmpty) false else math.abs(best - ep.score.min) < ep.epsilon.get
    } while(iterations > 0 && !termination)
    population
  }

  def bestScore(population: RDD[(Int, Agent)], score: BBOFunction): Double = {
    population.map(a => score(a._2.vector)).min()
  }

  def bestAgent(population: RDD[(Int, Agent)], score: BBOFunction): (Double, Agent) = {
    val best = population.map(a => (score(a._2.vector), a))
      .reduce((a, b) => if(a._1 < b._1) a else b)
    (best._1, best._2._2)
  }
}
