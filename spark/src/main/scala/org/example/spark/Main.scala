package org.example.spark

import org.apache.spark.{SparkConf, SparkContext}
import org.example._

object Main {
  def main(args: Array[String]): Unit = {
    val conf = new Conf(args)
    val sparkConf = conf.masterUrl.toOption match {
      case Some(url) => new SparkConf().setAppName("de-spark").setMaster(url)
      case None => new SparkConf().setAppName("de-spark")
    }
    val sc = new SparkContext(sparkConf)
    new DeSpark(conf.build).run(sc)
  }
}
