package org.example.flink

import org.apache.flink.api.common.functions.Partitioner
import org.apache.flink.api.scala._
import org.example.DifferentialEvolution.Population
import org.example.{Agent, DifferentialEvolution, ExecutionParameters}

class DeFlink(ep: ExecutionParameters) extends Serializable {

  def run(env: ExecutionEnvironment): Unit = {
    env.setParallelism(ep.islands)

    var data = DifferentialEvolution.buildPopulation(ep.population, ep.dimension, ep.score.domainMin, ep.score.domainMax)
    val deFunction = new DifferentialEvolution(ep.iterations, ep.score, ep.f, ep.cr)

    var termination = false
    var globalIterations = ep.globalIterations.getOrElse(Int.MaxValue)
    do {
      data = islandIterations(env.fromCollection(data), new RandomPartitioner, deFunction).collect()
      val best = bestAgent(data)
      println(s"Best agent: $best")
      globalIterations -= 1
      termination = if(ep.epsilon.isEmpty) false else math.abs(best._1 - ep.score.min) < ep.epsilon.get
    } while(globalIterations > 0 && !termination)
  }

  def islandIterations(data: DataSet[Agent], partitioner: Partitioner[Int], deFunction: (Population => Population)): DataSet[Agent] = {
    data.iterateWithTermination(ep.islandIterations) { input =>

      val out = input
        .partitionCustom(partitioner, _.id)
        .mapPartition(p => deFunction.apply(p.toSeq))

      val termination = if(ep.epsilon.isEmpty) out else bestScore(out).filter(a => math.abs(a - ep.score.min) > ep.epsilon.get)
      (out, termination)
    }
  }

  def bestScore(population: DataSet[Agent]): DataSet[Double] = {
    population.map(a => ep.score(a.vector)).reduce((a, b) => if(a < b) a else b)
  }

  def bestAgent(population: Population): (Double, Agent) = {
    population.map(a => (ep.score(a.vector), a)).reduce((a, b) => if(a._1 < b._1) a else b)
  }
}
