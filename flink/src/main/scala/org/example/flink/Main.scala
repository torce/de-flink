package org.example.flink

import org.apache.flink.api.scala._
import org.example.Conf

object Main extends App {
  new DeFlink(new Conf(args).build).run(ExecutionEnvironment.getExecutionEnvironment)
}
