package org.example.flink

import org.apache.flink.api.common.functions.Partitioner

import scala.util.Random

class RandomPartitioner extends Partitioner[Int] {
  override def partition(key: Int, numPartitions: Int): Int = Random.nextInt(numPartitions)
}

