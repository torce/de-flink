package org.example.flink

import org.apache.flink.api.scala._
import org.apache.flink.core.fs.FileSystem

object WordCount extends App {
  val env = ExecutionEnvironment.getExecutionEnvironment

  val textFile = env.readTextFile(args(0))
  val counts = textFile.flatMap(line => line.split(" "))
    .map(word => (word, 1))
    .groupBy(0)
    .sum(1)
  counts.writeAsText(args(1), FileSystem.WriteMode.OVERWRITE)

  env.execute()
}
