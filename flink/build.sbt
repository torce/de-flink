resolvers in ThisBuild ++= Seq(Resolver.mavenLocal)

name := """flink"""

version := "1.0"

scalaVersion := "2.10.5"

val flinkVersion = "1.0.0"

val flinkDependencies = Seq(
  "org.apache.flink" %% "flink-scala" % flinkVersion % "provided",
  "org.apache.flink" %% "flink-streaming-scala" % flinkVersion % "provided")

libraryDependencies ++= flinkDependencies

// exclude Scala library from assembly
assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)