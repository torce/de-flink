#!/bin/bash
mkdir -p results
ssh david.pereiro@pluton.des.udc.es "zip -r BDEv_OUT.zip BDEv_OUT"
scp david.pereiro@pluton.des.udc.es:/home/david.pereiro/BDEv_OUT.zip results/
unzip results/BDEv_OUT.zip -d results/