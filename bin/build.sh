#!/bin/bash
sbt "project spark" "assembly" "project flink" "assembly"
scp spark/target/scala-2.10/spark-assembly-1.0.jar david.pereiro@pluton.des.udc.es:/home/david.pereiro/exec
scp flink/target/scala-2.10/flink-assembly-1.0.jar david.pereiro@pluton.des.udc.es:/home/david.pereiro/exec