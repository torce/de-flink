#!/usr/bin/env ruby
require 'yaml'
require 'gruff'

def plot_results(summary, file)
  sizes = {0 => 1, 1 => 2, 2 => 4, 3 => 8}
  g = Gruff::Line.new
  g.title = 'Summary'
  g.labels = {0 => 1, 1 => 2, 2 => 4, 3 => 8}
  summary.group_by {|_, v| [v[:type], v[:nodes], v[:tool], v[:function]]}.each do |(type, nodes, tool, function), reports|
    data = sizes.map do |_, size|
      valid = reports.select {|_, r| r[:islands].to_i == size}
      if valid.size > 1
        puts 'Several results for the same configuration'
        puts "tool: #{tool}, type: #{type}, nodes: #{nodes}, islands: #{size}"
        puts valid.map(&:first)
      elsif valid.empty?
        puts 'Empty values for configuration'
        puts "tool: #{tool}, type: #{type}, nodes: #{nodes}, islands: #{size}, function: #{function}"
      else
        valid.first.last[:avg_times]
      end
    end
    g.data("#{type}-#{nodes}n-#{tool}-#{function}", data)
  end
  g.write("results/#{file}")
end

report = YAML.load(File.open('results/summary.yml'))

plot_results(report.select {|_, v| v[:type] == :effort}, 'effort.png')
plot_results(report.select {|_, v| v[:type] == :vtr}, 'vtr.png')