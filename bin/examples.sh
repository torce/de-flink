#!/usr/bin/env bash
rm -rf resources/quijote-spark-words.txt
/home/david/opt/spark-1.6.2/bin/spark-submit \
  --class org.example.spark.WordCount \
  --master local \
  spark/target/scala-2.10/spark-assembly-1.0.jar \
  resources/quijote.txt \
  resources/quijote-spark-words.txt

rm -rf resources/quijote-flink-words.txt
/home/david/opt/flink-0.10.2/bin/flink run \
  --class org.example.flink.WordCount \
  flink/target/scala-2.10/flink-assembly-1.0.jar \
  /home/david/repos/de-flink/resources/quijote.txt \
  /home/david/repos/de-flink/resources/quijote-flink-words.txt