#!/usr/bin/env ruby
require 'yaml'

def parse_agent(agent)
  match = /\((.+),Agent\(.+,(?:List|Vector)\((.+)\)\)\)/.match(agent)
  return unless match
  score, agent = match.captures
  [score.to_f, agent.split(', ').map(&:to_f)]
end

def get_scores(report_dir)
  scores = Hash.new {|h, k| h[k] = []}
  Dir.glob("#{RESULTS}/#{report_dir}/**/command*/output").each do |output|
    i = /command_(\d+)/.match(output).captures.first.to_i
    agents = `grep #{output} -e 'Best agent: '`.split("\n")
    scores[i] << agents.map {|a| parse_agent(a)}.compact.map(&:first)
  end
  scores.sort.to_h
end

def get_times(report_dir)
  times = {}
  Dir.glob("#{RESULTS}/#{report_dir}/**/command*/elapsed_time").each do |file|
    i = /command_(\d+)/.match(file).captures.first.to_i
    times[i] = File.read(file).to_f
  end
  times.sort.to_h
end

def get_command(report_dir)
  `grep #{RESULTS}/#{report_dir}/log -e 'Running spark' -e 'Running flink'`.split("\n").first
end

def parse_command(command)
  regex = /Running (?<tool>[a-z]+).*--function (?<function>\d+).*--islands (?<islands>\d+)/
  matches = command.match(regex)
  matches.names.map(&:to_sym).zip(matches.captures).to_h
end

def get_nodes(report_dir)
  `grep #{RESULTS}/#{report_dir}/summary -Pe "Cluster sizes.+(\\d+)"`.split.last.to_i - 1
end

def get_type(command)
  command =~ /epsilon/ ? :vtr : :effort
end

def process_scores(scores)
  # Gets the first 10 of the list of scores and groups them per iteration
  return if scores.nil? || scores.size < 10
  formatted = scores.map(&:last).map(&:first)
  max_size = formatted.map(&:size).max
  formatted.map {|s| s.values_at(0...max_size)}.transpose.map(&:compact)
end

def valid_data(report, times, scores)
  valid_times, valid_scores = times.values.zip(scores).select {|_, (_, s)| s.any?(&:any?)}.transpose
  puts "#{report} has less than 15 correct evaluations" if valid_times.size < 15
  [valid_times.size, valid_times[5...15], process_scores(valid_scores[5...15])]
end

def average(samples)
  samples.inject(:+) / samples.size
end

def sample_standard_deviation(samples, avg)
  sum = samples.inject(0) {|acc, v| acc + (v - avg) ** 2}
  Math.sqrt((sum / (samples.size - 1)).to_f)
end

RESULTS = 'results/BDEv_OUT'
OUTPUT = 'results/summary.yml'

summary = Hash.new {|h, k| h[k] = Hash.new(&h.default_proc)}
Dir.entries(RESULTS).select {|e| e =~ /^report/}.map do |report|
  command = get_command(report)
  scores = get_scores(report)
  times = get_times(report)
  valid, valid_times, valid_scores = valid_data(report, times, scores)

  summary[report] = parse_command(command)
  summary[report][:type] = get_type(command)
  summary[report][:nodes] = get_nodes(report)
  summary[report][:command] = command
  summary[report][:valid] = valid
  summary[report][:all_times] = times
  if valid_times && valid_times.size >= 10
    avg = average(valid_times)
    sd = sample_standard_deviation(valid_times, avg)
    avg_scores = valid_scores.map {|s| average(s)}
    sd_scores = valid_scores.zip(avg_scores).map {|s, a| sample_standard_deviation(s, a)}
    summary[report][:times] = valid_times
    summary[report][:avg_times] = avg
    summary[report][:sd_times] = sd
    summary[report][:scores] = valid_scores
    summary[report][:avg_scores] = avg_scores
    summary[report][:sd_scores] = sd_scores
  end
end

sorted = summary.sort_by {|_, v| [v[:type], v[:tool], v[:function], v[:nodes], v[:islands]]}

File.write(OUTPUT, sorted.to_yaml)

summary.select {|_, v| v[:valid] < 15}.each do |report, results|
  puts "#{report}: #{results[:tool]}-#{results[:type]}-#{results[:nodes]}-#{results[:islands].to_i / results[:nodes]}-#{results[:function]}"
end