#!/usr/bin/env ruby
require 'yaml'
require 'erb'

rows = YAML.load(File.open('results/summary.yml')).map(&:last)
File.write('bin/latex_table.tex', ERB.new(File.read('bin/latex_table.erb'), nil, '-').result(binding))