#!/usr/bin/env ruby
require 'yaml'
require 'csv'

def generate_csv(type, summary)
  times = CSV.open("results/#{type}_times.csv", 'wb', write_headers: true, headers: %w[tool function nodes islands time])
  scores = CSV.open("results/#{type}_scores.csv", 'wb', write_headers: true, headers: %w[tool function nodes islands iteration score])
  summary.group_by do |_report, data|
    [data[:tool], data[:function], data[:nodes], data[:islands]]
  end.each do |header, group|
    raise "Duplicated value for #{header}" if group.size > 1
    _report_name, data = group.first
    data[:times].each {|t| times << [*header, t]} if data[:times]
    data[:scores].each_with_index {|s, i| s.each {|v| scores << [*header, i, v]}} if data[:scores]
  end
  times.close
  scores.close
end

summary = YAML.load_file('results/summary.yml')
summary.group_by {|_, d| d[:type]}.each {|t, s| generate_csv(t, s)}

